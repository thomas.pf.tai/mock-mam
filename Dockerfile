FROM node:11-alpine
MAINTAINER Thomas Tai <articwind9653@gmail.com>

# install jason-server
RUN npm install -g json-server

WORKDIR /data
VOLUME /data

COPY ./*.json ./

EXPOSE 3000
ADD run.sh /run.sh
ENTRYPOINT [ "sh", "/run.sh" ]
CMD []