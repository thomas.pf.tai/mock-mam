#!/bin/bash

args="$@"

args="$@ -p 80 --host 0.0.0.0"

file=/data/values.json
if [ -f $file ]; then
    echo "Found values.json, trying to open"
    args="$args values.json"
fi

file=/data/file.js
if [ -f $file ]; then
    echo "Found file.js seed file, trying to open"
    args="$args file.js"
fi

file=/data/routes.json
if [ -f $file ]; then
	echo "Found custom routes, trying to open"
	args="$args --routes routes.json"
fi

json-server $args